#xdiv

 xdiv是一个单页应用框架~目前主要用于HTML5 HybridApp 
 现在功能是可以将其他html中代码引入到主html 使单页应用代码易于维护
依赖JQuery和Animate.css  若不需特效可以不引用Animate.css

------------------------2015-11-11 version 0.0.2 ----------------------------- 光棍节快乐
 - 添加css和js引用(javascript)
	 ` importJS("js/index.js");
	 importCSS("css/index.js");`

 - 根据json文件引用
	- dependency.js 文件名是自定义的
	- 内容如:
	- [{
	- 	"path":"css/animate.min.css",
	-	"type":"css",
	-	"remark":"animate 特效"
	- },{
	-	"path":"css/bootstrap.min.css",
	-	"type":"css"
	-	"remark":"bootstrap"
	- },{
	-	"path":"js/animate.js",
	-	"type":"js"
	-	"remark":"特效应用"
	- }]
	- remark是备注~可空
	
	- 在需要引入的页面添加属性 x-import="dependency.json"
	- 如: ` <html x-import="dependency.json"> `
	
	- 建议:
	- 1.此属性放在html或者head中(因为涉及文件引用,应尽量靠前)
	- 2.由于依赖JQ 所以需要先引用jq再引用xdiv.js

------------------------2015-10-23 version 0.0.1 -----------------------------
- 建议页面使用层叠方式布局.
 - 如: ` <div id="page1" x-div="index.html" page-name="index" isAsync="true"> `  
`isAsync` 是否异步  默认是同步的,不过会导致刚入页面样式有问题,可以通过加入加载页面解决
`isAsync="true"` 异步. 由于页面代码加载为异步,有可能导致事件注册失败.

不一定是div 可以是任意标签 
xdiv和page-name属性必须有 属性值为对应页面  page-name需要有~跳转页面时使用page-name跳转
`
/**
 * 页面跳转
 * @param {Object} fromName 当前页面
 * @param {Object} toName 目标页面
 * @param {Object} outAnimation 移出特效
 * @param {Object} inAnimation 移入特效
 * @param {Object} delay  移出和移入特效的延时
 */
function toPage(fromName, toName, outAnimation, inAnimation, delay)
`

